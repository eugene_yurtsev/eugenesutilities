import sys
from pandas import DataFrame
import git
import os
from numpy import array

def get_loaded_git_info():
    """
    For all loaded modules: If the module is under git source control,
    the git working path is recorded together with the git commit, and whether the repository is dirty.

    Returns
    ---------
    Dictionary : keys are git repository paths, values contain all the information
    """
    git_map = {}

    for k, v in sys.modules.items():
        if hasattr(v, '__path__'):
            try:
                path = v.__path__[0]
                repo = git.Repo(path)
                assert repo.bare == False
                git_map[repo.working_dir] = (repo.working_dir, os.path.split(repo.working_dir)[1],
                            str(repo.active_branch), str(repo.commit()), repo.is_dirty())
            except git.InvalidGitRepositoryError:
                pass
    return git_map


def show_source_versions():
    """ Use to render an IPython HTML table of git repositories associated with loaded python modules. """
    git_map = get_loaded_git_info()
    data = git_map.values()
    d = DataFrame(array(data), columns=['Repository Path', 'Folder Name', 'Branch', 'Commit #', 'Dirty?'])
    from IPython.display import HTML
    return HTML(d.to_html(index=False))
